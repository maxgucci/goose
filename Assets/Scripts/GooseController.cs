﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooseController : MonoBehaviour
{
    private float speed = 1.5f;
    private float turnAroundSpeed = 220;

    private float exitTime = 0;

    [SerializeField]
    private Animator animator;
    [SerializeField]
    private CharacterController charController;

    private float velocity;

    private float accel = 1;

    public Transform mouthEnd;
    public Transform mouthEndFinish;

    private Item item;
    private Vector3 itemPoint;
    private float dropDist = 0.9f;

    public string info;

    private Vector3 targetRotation;

    public System.Action<Item> onItemTaked;
    public System.Action<Item> onItemDroped;
    public System.Action<Interface> onInterracted;

    private Vector3 lastKnownInput;

    private void Start()
    {
        targetRotation = transform.forward;
        lastKnownInput = transform.forward;
    }

    void Update ()
    {
        Controller();
	}

    private void Controller()
    {
        info = "";

        Vector3 view = Camera.main.transform.forward;
        view.y = 0;
        view.Normalize();
        float angleShift = Vector3.SignedAngle(view, Vector3.forward, Vector3.up) + 90;

        info += angleShift + " ";

        float v = InputManager.Instance().GetAxis("Vertical");
        float h = InputManager.Instance().GetAxis("Horizontal");

        Vector3 dir = new Vector3(-v, 0, h);
        float s = dir.magnitude;
        if(s > 1) s = 1;
        if (s > 0.1f) lastKnownInput = dir;

        dir.Normalize();

        dir = Quaternion.AngleAxis(angleShift, Vector3.up) * dir;
        if (s > 0.1f) targetRotation = dir;

        info += " " + lastKnownInput + " " + targetRotation;

        if (InputManager.Instance().GetButton ("Run")) accel = Mathf.Lerp(accel, 2.0f, 5 * Time.deltaTime);
        else accel = Mathf.Lerp(accel, 1, 5 * Time.deltaTime);

        animator.SetFloat("Speed", accel * s);

        transform.forward = Vector3.Lerp(transform.forward, targetRotation, 10 * Time.deltaTime);

        Vector3 fall = new Vector3();
        if (!charController.isGrounded) fall.y = -1f;

        charController.Move((transform.forward * accel * s * speed + fall) * Time.deltaTime);

        info += "" + charController.isGrounded;

        //if (Input.GetMouseButtonDown(0))
        //{
        //    animator.SetTrigger("Attack");
        //}

        //if (Input.GetMouseButtonDown(1))
        //{
        //    animator.SetTrigger("Take");
        //}

        if (canTakeTimer > 0) canTakeTimer -= Time.deltaTime;

        if (canTakeTimer <= 0 && InputManager.Instance().GetButtonDown ("Use"))
        {
            animator.SetTrigger("Take");
            canTakeTimer = 2.5f;
        }

        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var list = Physics.SphereCastAll(ray.origin, 0.2f, ray.direction);
            for (int i = 0; i < list.Length; i++)
            {
                Item item = list[i].transform.GetComponent<Item>();
                Interface interf = list[i].transform.GetComponent<Interface>();
                Transform tr = null;
                if (interf != null) tr = interf.transform;
                if (item != null) tr = item.transform;
                
                if ((tr != null && tr.gameObject.activeSelf))
                {
                    ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(mouthEndFinish.position));

                    dir = tr.transform.position - transform.position;
                    dir.y = 0;
                    float dist = dir.magnitude;
                    dir.Normalize();

                    if(Mathf.Abs(ray.direction.y) > 0.01f)
                    {
                        float y = tr.transform.position.y;
                        float a = (y - ray.origin.y) / ray.direction.y;
                        Vector3 p = ray.origin + a * ray.direction;

                        p1 = p;
                        p2 = tr.transform.position;

                        Vector3 dir2 = p - transform.position;
                        dir2.y = 0;
                        dir2.Normalize();

                        angleShift = Vector3.SignedAngle(dir2, transform.forward, Vector3.up);

                        dir = Quaternion.AngleAxis(angleShift, Vector3.up) * dir;
                    }

                    targetRotation = dir;
                    animator.SetTrigger("Take");

                    //UnityEditor.EditorApplication.isPaused = true;
                    break;
                }
            }
        }

        if(item != null)
        {
            float dist = Vector3.Distance(item.transform.position, mouthEnd.position);
            if (dist > dropDist)
            {
                this.item.transform.SetParent(null);
                var rb = this.item.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.isKinematic = false;
                    //rb.velocity = ray.direction * 3;
                }

                if (onItemDroped != null) onItemDroped(item);
                this.item = null;
                Debug.Log("Drop " + dist);
            }
        }

        if(item != null)
        {
            dir = mouthEnd.position - item.transform.TransformPoint(itemPoint);
            float dist = dir.magnitude;
            dir.Normalize();
            if(dist > 0.01f)
            {
                Vector3 pos1 = item.transform.TransformPoint(itemPoint);
                Vector3 pos2 = Vector3.Lerp(pos1, mouthEnd.position, 10 * Time.deltaTime);
                dir = pos2 - pos1;
                item.transform.position += dir;
            }
        }
    }

    private Vector3 p1;
    private Vector3 p2;
    private void OnDrawGizmos()
    {
        if (item != null)
        {
            Gizmos.DrawLine(mouthEnd.position, item.transform.TransformPoint(itemPoint));
        }

        Gizmos.DrawLine(transform.position, p1);

        Gizmos.DrawLine(transform.position, p2);
    }

    private float canTakeTimer;
    public void CanTake ()
    {
        canTakeTimer = 0;
    }

    public void OnTake ()
    {
        var ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(mouthEnd.position));

        if (this.item == null)
        {
            var list = Physics.SphereCastAll(ray, 0.25f);

            //Debug.Log("Casted " + list.Length);
            //for (int i = 0; i < list.Length; i++) Debug.Log(list[i].transform, list[i].transform);

            for (int i = 0; i < list.Length; i++)
            {
                Item item = list[i].transform.GetComponent<Item>();
                if (item != null && item.gameObject.activeSelf)
                {
                    this.item = item;
                    item.transform.SetParent(mouthEnd);
                    itemPoint = item.transform.InverseTransformPoint(list[i].point);
                    var rb = item.GetComponent<Rigidbody>();
                    if (rb != null) rb.isKinematic = true;
                    item.Taked();
                    if (onItemTaked != null) onItemTaked(item);
                    dropDist = Vector3.Distance(item.transform.position, mouthEnd.position) + 0.1f;
                    //UnityEditor.EditorApplication.isPaused = true;
                    Debug.Log("Item taked " + item + " " + dropDist, item);
                    break;
                }

                Interface interf = list[i].transform.GetComponent<Interface>();
                if (interf != null && interf.gameObject.activeSelf)
                {
                    interf.Interract();
                    if (onInterracted != null) onInterracted(interf);
                }
            }
        }
        else
        {
            this.item.transform.SetParent(null);
            var rb = this.item.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.isKinematic = false;
                rb.velocity = ray.direction * 3;
            }

            if (onItemDroped != null) onItemDroped(item);
            this.item = null;
            Debug.Log("Drop");
        }
    }
}
