﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string Name;
    public string RussianName;

    public float weight = 1f;

    public System.Action<Item> onTake;

    private Transform parent;
    private Vector3 localPosition;

    // Start is called before the first frame update
    void Start()
    {
        parent = transform.parent;
        localPosition = transform.localPosition;
    }

    public void Taked ()
    {
        if (onTake != null) onTake(this);
    }

    public void Return()
    {
        transform.SetParent(parent);
        transform.localPosition = localPosition;
    }

    public float DistanceFromPlace()
    {
        Vector3 dir = transform.position - localPosition;
        if(parent != null) dir = transform.position - parent.TransformPoint(localPosition);

        return dir.magnitude;
    }
}
