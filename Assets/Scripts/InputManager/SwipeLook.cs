﻿using UnityEngine;
using System.Collections;

public class SwipeLook : MonoBehaviour {

	//public
	private float minDelta = 1;

	public RectTransform zone;

	// Use this for initialization
	void Start () {
	
		minDelta = (Screen.width + Screen.height)/2.0f;
		minDelta /= 20.0f;
		minDelta *= minDelta;
	}

	private bool IsPointInZone(Vector2 point) {
	
		Rect rect = zone.rect;

		float x1 = zone.position.x - rect.width * zone.lossyScale.x * zone.pivot.x;
		float x2 = zone.position.x + rect.width * zone.lossyScale.x * (1 - zone.pivot.x);
		float y1 = zone.position.y - rect.height * zone.lossyScale.y * zone.pivot.y;
		float y2 = zone.position.y + rect.height * zone.lossyScale.y * (1 - zone.pivot.y);

		return point.x > x1 && point.x < x2 && point.y > y1 && point.y < y2;
	}

	private bool waitingTouches = true;
	// Update is called once per frame

	public UnityEngine.UI.Text lblInfo;

	void Update () {
	
		#if UNITY_EDITOR

		if (Input.GetMouseButtonDown(0)) {
			if (waitingTouches && IsPointInZone(Input.mousePosition)) {
				waitingTouches = false;
				PointerDown (Input.mousePosition);
			}
		}

		if(Input.GetMouseButton(0) && !waitingTouches) {
			Vector2 delta = Input.mousePosition;
			delta -= prevPos;
			OnDrag (delta);
			prevPos = Input.mousePosition;
		}

		if(Input.GetMouseButtonUp(0)) {
			if(!waitingTouches) {
				PointerUp(Input.mousePosition);
				waitingTouches = true;
			}
		}//*/
		
		#else
		if (Input.touches.Length > 0) {
			
			if (waitingTouches) {

				if(lblInfo != null) lblInfo.text = "Waiting touches " + Input.touches.Length;

				for (int i = 0; i < Input.touches.Length; i++) {
					
					if (IsPointInZone(Input.touches [i].position)) {
						touch = Input.touches [i];
						PointerDown (touch.position);
						if(lblInfo != null) lblInfo.text = "Found touch " + Input.touches [i].position + " " + Input.touches [i].fingerId + " " + touch.position + " " + touch.fingerId;
					}
				}

			} else {

				bool found = false;
				for (int i = 0; i < Input.touches.Length; i++) {
				
					if (Input.touches [i].fingerId == touch.fingerId) {
						found = true;
						touch = Input.touches [i];
						OnDrag (Input.touches [i].position - prevPos);
						prevPos = Input.touches [i].position;
						break;
					}
				}

				if (!found) {
					PointerUp (touch.position);
				} else
					if(lblInfo != null) lblInfo.text = "Found " + touch.position;
			}
		
		} else {

			if(lblInfo != null) lblInfo.text = "0 touches";

			waitingTouches = true;
			OnDrag (Vector2.zero);
		}
		#endif
	}

	private Vector2 startPoint;
	private Touch touch;
	//private bool down;
	private Vector2 prevPos;
	private void PointerDown(Vector2 coord) {
	
		startPoint = coord;
		prevPos = startPoint;
		//down = true;
		waitingTouches = false;
	}

	private void PointerUp(Vector2 coord) {
	
		UpdateVirtualAxes(Vector2.zero);
		//down = false;
		waitingTouches = true;
	}

	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public float sens = 1;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public bool invertX = false;
	public bool invertY = false;
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	InputManager.Axis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	InputManager.Axis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

	void OnEnable()
	{
		CreateVirtualAxes();
	}

	/*void LateUpdate() {

		UpdateVirtualAxes(Vector2.zero);
	}//*/

	void UpdateVirtualAxes(Vector2 delta)
	{

		if (m_UseX)
		{
			if(invertX)
				m_HorizontalVirtualAxis.Set(sens*delta.x);
			else 
				m_HorizontalVirtualAxis.Set(-sens*delta.x);
		}

		if (m_UseY)
		{
			if(invertY) 
				m_VerticalVirtualAxis.Set(-sens*delta.y);
			else
				m_VerticalVirtualAxis.Set(sens*delta.y);
		}
	}

	void CreateVirtualAxes()
	{
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new InputManager.Axis(horizontalAxisName);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new InputManager.Axis(verticalAxisName);
		}
	}


	public void OnDrag(Vector2 delta)
	{
		UpdateVirtualAxes(delta);
	}

	void OnDisable()
	{
		// remove the joysticks from the cross platform input
		if (m_UseX)
		{
			//m_HorizontalVirtualAxis.Remove();
		}
		if (m_UseY)
		{
			//m_VerticalVirtualAxis.Remove();
		}
	}

	public void ToogleInvertX(bool b) {

		invertX = !invertX;
	}

	public void ToogleInvertY(bool b) {

		invertY = !invertY;
	}
}
