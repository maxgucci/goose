this package include:

Duck animated 3d model

6 Animations
2 Textures(2048x2048)
1 Prefab

2668 quad polygons on this mesh
2670 vertices on this mesh

Textures

duck_D.png
duck_N.png

Animations list

0-24 duck_walk
27-200 duck_idle
203-263 duck_eat
266-275 duck_lay
278-302 duck_swim
305-314 duck_rise

enjoy:)
