﻿using UnityEngine;
using System.Collections.Generic;
#if CROSS_PLATFORM_INPUT
using UnityStandardAssets.CrossPlatformInput;
#endif

public class InputManager : MonoBehaviour {

	public class Axis {
	
		private string name;

		public Axis(string name) {
		
			this.name = name;

			InputManager.Instance().AddAxis(name);
		}

		public float Get() {
		
			return InputManager.Instance ().GetAxis (name);
		}

		public void Set(float val) {
		
			InputManager.Instance ().SetAxis (name, val);

			#if CROSS_PLATFORM_INPUT && !UNITY_STANDALONE
			CrossPlatformInputManager.SetAxis(name, val);
			#endif
		}

		public void Update(float delta) {
		
			float val = (InputManager.Instance ().GetAxis (name) + delta);
			InputManager.Instance ().SetAxis (name, val);

			#if CROSS_PLATFORM_INPUT && !UNITY_STANDALONE
			CrossPlatformInputManager.SetAxis(name, val);
			#endif
		}
	}


	public class Button {
	
		private string name;

		public Button (string name) {
		
			this.name = name;
			InputManager.Instance().AddButton(name);
		}

		public bool Get() {

			return InputManager.Instance ().GetButton (name);
		}

		public void Set(bool val) {

			InputManager.Instance ().SetButton (name, val);
		}
	}

	private static InputManager instance;

	public static InputManager Instance() {
	
		if (instance == null)
			instance = FindObjectOfType<InputManager>();

		if(instance == null) {
			GameObject go = new GameObject("InputManager");
			instance = go.AddComponent<InputManager>();
			instance.axes = new List<string>();
			instance.buttons = new List<string> ();
		}

		return instance;
	}

	void Awake() {

		if (instance == null)
			instance = this;
		else if(instance != this) {
			Destroy (this);
		}
	}

	public List<string> axes;

	public List<float> values = new List<float>();

	public List<string> buttons;

	public List<bool> buttonsStates = new List<bool>();
	public List<bool> buttonsDown = new List<bool>();
	private List<bool> buttonsUp = new List<bool> ();

	// Use this for initialization
	void Start () {
	
		for (int i = 0; i < axes.Count; i++)
			values.Add (0);

		for (int i = 0; i < buttons.Count; i++) {
			buttonsStates.Add (false);
			buttonsDown.Add (false);
			buttonsUp.Add (false);
		}
	}

	void LateUpdate() {

		//Debug.Log ("LateUpdate");

		for (int i = 0; i < buttonsUp.Count; i++)
			buttonsUp [i] = false;

		for (int i = 0; i < buttonsDown.Count; i++) {
			//if (buttonsDown [i]) Debug.Log ("Drop " + buttons [i]);
			buttonsDown [i] = false;
		}

		foreach (var kvp in toSet)
			LateSetButton (kvp.Key, kvp.Value);

		toSet.Clear ();
	}
	
	public float GetAxis(string name) {

		if(!axes.Contains(name)) return 0;

		int ind = 0;
		for (; ind < axes.Count; ind++)
			if (axes [ind] == name)
				break;

		if (ind >= values.Count) {
			for(int i = values.Count; i < axes.Count; i++)
				values.Add(0);
		}

		return values [ind];
	}

	public float GetAxis(int id) {

		if(id <0 || id >= axes.Count) return 0;

		int ind = id;

		if (ind >= values.Count) {
			for(int i = values.Count; i < axes.Count; i++)
				values.Add(0);
		}

		return values [ind];
	}

	public int GetAxisId (string name) {
	
		return axes.IndexOf (name);
	}

	public void SetAxis(string name, float v) {
		
		if(!axes.Contains(name)) return;
		
		int ind = 0;
		for (; ind < axes.Count; ind++)
			if (axes [ind] == name)
				break;
		
		if (ind >= values.Count) {
			for(int i = values.Count; i < axes.Count; i++)
				values.Add(0);
		}
		
		values [ind] = v;
	}

	public void AddAxis(string name) {

		if(axes.Contains(name)) return;

		axes.Add (name);
		values.Add (0);
	}

	public bool GetButton(string name) {

		if (!buttons.Contains (name)) {
			return false;
		}

		int ind = 0;
		for (; ind < buttons.Count; ind++)
			if (buttons [ind] == name)
				break;

		if (ind >= buttonsStates.Count) {
			for (int i = buttonsStates.Count; i < buttons.Count; i++) {
				buttonsStates.Add (false);
				buttonsDown.Add (false);
				buttonsUp.Add (false);
			}
		}
			
		return buttonsStates [ind];
	}

	public bool GetButtonDown(string name) {
	
		if (!buttons.Contains (name)) {
			//Debug.Log ("Not contains");
			return false;
		}

		int ind = 0;
		for (; ind < buttons.Count; ind++)
			if (buttons [ind] == name)
				break;

		if (ind >= buttonsStates.Count) {
			for (int i = buttonsStates.Count; i < buttons.Count; i++) {
				buttonsStates.Add (false);
				buttonsDown.Add (false);
				buttonsUp.Add (false);
			}
		}

		//if (buttonsDown [ind]) Debug.Log (name + " " + true);
		
		return buttonsDown [ind];
	}

	public bool GetButtonUp(string name) {

		if(!buttons.Contains(name)) return false;

		int ind = 0;
		for (; ind < buttons.Count; ind++)
			if (buttons [ind] == name)
				break;

		if (ind >= buttonsStates.Count) {
			for (int i = buttonsStates.Count; i < buttons.Count; i++) {
				buttonsStates.Add (false);
				buttonsDown.Add (false);
				buttonsUp.Add (false);
			}
		}

		return buttonsUp [ind];
	}

	private Dictionary<string, bool> toSet = new Dictionary<string, bool>();
	public void SetButton(string name, bool v) {

		if (!buttons.Contains (name)) {
			return;
		}

		if (!toSet.ContainsKey (name))
			toSet.Add (name, false);

		toSet [name] = toSet[name] || v;
	}

	private void LateSetButton(string name, bool v) {

		if (!buttons.Contains (name)) {
			return;
		}

		//if (v) Debug.Log ("Set " + name);
		
		int ind = 0;
		for (; ind < buttons.Count; ind++)
			if (buttons [ind] == name)
				break;

		if (ind >= buttonsStates.Count) {
			for (int i = buttonsStates.Count; i < buttons.Count; i++) {
				buttonsStates.Add (false);
				buttonsDown.Add (false);
				buttonsUp.Add (false);
			}
		}
			
		if(buttonsStates[ind] && !v) buttonsUp[ind] = true;
		if(!buttonsStates[ind] && v) buttonsDown[ind] = true;
		buttonsStates [ind] = v;
	}

	public void AddButton(string name) {

		if(buttons.Contains(name)) return;

		buttons.Add (name);
		buttonsStates.Add (false);
		buttonsDown.Add (false);
		buttonsUp.Add (false);
	}
}

