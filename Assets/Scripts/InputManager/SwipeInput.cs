﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public float sens = 1;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public bool invertX = false;
	public bool invertY = false;
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	InputManager.Axis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	InputManager.Axis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

	void OnEnable()
	{
		CreateVirtualAxes();
	}

	void Start()
	{

	}

	void LateUpdate() {
	
		UpdateVirtualAxes(Vector2.zero);
	}

	void UpdateVirtualAxes(Vector2 delta)
	{

		if (m_UseX)
		{
			if(invertX)
				m_HorizontalVirtualAxis.Set(sens*delta.x);
			else 
				m_HorizontalVirtualAxis.Set(-sens*delta.x);
		}

		if (m_UseY)
		{
			if(invertY) 
				m_VerticalVirtualAxis.Set(-sens*delta.y);
			else
				m_VerticalVirtualAxis.Set(sens*delta.y);
		}
	}

	void CreateVirtualAxes()
	{
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new InputManager.Axis(horizontalAxisName);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new InputManager.Axis(verticalAxisName);
		}
	}


	public void OnDrag(PointerEventData data)
	{
		UpdateVirtualAxes(data.delta);
	}


	public void OnPointerUp(PointerEventData data)
	{
		UpdateVirtualAxes(Vector2.zero);
	}


	public void OnPointerDown(PointerEventData data) {

	}

	void OnDisable()
	{
		// remove the joysticks from the cross platform input
		if (m_UseX)
		{
			//m_HorizontalVirtualAxis.Remove();
		}
		if (m_UseY)
		{
			//m_VerticalVirtualAxis.Remove();
		}
	}

	public void ToogleInvertX(bool b) {

		invertX = !invertX;
	}

	public void ToogleInvertY(bool b) {

		invertY = !invertY;
	}
}