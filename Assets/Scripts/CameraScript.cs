﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private GameObject goose;

    private Vector3 startCamPosition;
    private Vector3 startGoosePosition;

    private float deltaX;
    private float deltaZ;

    //   private Vector3 startPos;
    //   public Transform goosePos;

    void Start()
    {
        startCamPosition = transform.position;
        startGoosePosition = goose.transform.position;
    }

    //void Update ()
    //   {
    //       transform.position = startPos;
    //}

    private void Update()
    {
        deltaX = goose.transform.position.x - startGoosePosition.x;
        deltaZ = goose.transform.position.z - startGoosePosition.z;

        transform.position = new Vector3(startCamPosition.x + deltaX, startCamPosition.y, startCamPosition.z + deltaZ);
        //transform.position = new Vector3(transform.position.x - goose.transform.position.x, transform.position.y - goose.transform.position.y, transform.position.z);
    }
}
