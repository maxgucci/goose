﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interface : MonoBehaviour
{
    public string Name;
    public string RussianName;

    public UnityEvent onInterract;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Interract ()
    {
        if (onInterract != null) onInterract.Invoke();
    }
}
