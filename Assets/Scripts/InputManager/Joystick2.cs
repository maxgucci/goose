﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joystick2 : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public enum Size {
		current,
		auto,
		set,
	}

	public float sens = 1;
	private float MovementRange = 100;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public bool invertX = false;
	public bool invertY = false;
	//public bool holdX = false;
	//public bool holdY = false;
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

	Vector2 startPos;
	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	InputManager.Axis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	InputManager.Axis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

	public RectTransform border;
	public RectTransform joystick;
	private RectTransform rt;

	public Size sizeType;
	public float size = 100;

	private Vector2 joystickPos;
	private Vector2 borderPos;

	public bool hide;

	private bool returnPos = true;

	void OnEnable() {
		
		CreateVirtualAxes();
	}

	void CreateVirtualAxes() {
		
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new InputManager.Axis(horizontalAxisName);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new InputManager.Axis(verticalAxisName);
		}
	}

	public void ToogleInvertX(bool b) {

		invertX = !invertX;
	}

	public void ToogleInvertY(bool b) {

		invertY = !invertY;
	}

	private float scale = 1;

	private Vector3 origin;

	private bool horizontalAxisExists;
	private bool verticalAxisExists;

	void Start() {
	
		horizontalAxisExists = AxisExists (horizontalAxisName);
		verticalAxisExists = AxisExists (verticalAxisName);

		rt = GetComponent<RectTransform> ();

		if (sizeType == Size.current) {
			size = Mathf.Min (joystick.rect.width, joystick.rect.height);
		} else if (sizeType == Size.auto) {
			size = Mathf.Sqrt (Screen.width * Screen.height) / 10;
		}

		MovementRange = size;

		border.anchorMin = Vector2.zero;
		border.anchorMax = Vector2.zero;
		border.sizeDelta = new Vector2(2*size, 2*size);
		//border.anchoredPosition = Vector2.zero;

		Vector3 pos2 = joystick.localPosition;
		joystick.anchorMin = Vector2.zero;
		joystick.anchorMax = Vector2.zero;
		joystick.sizeDelta = new Vector2(size, size);
		joystick.localPosition = pos2;
		joystickPos = joystick.anchoredPosition;
		//joystick.anchoredPosition = Vector2.zero;

		Vector2 rect = rt.rect.size;
		rect.Scale (rt.pivot);
		origin = rt.position - new Vector3(rect.x, rect.y, 0);

		borderPos = joystickPos;
		border.anchoredPosition = borderPos;


		if (hide) border.gameObject.SetActive (false);
		if (hide) joystick.gameObject.SetActive (false);

		Canvas canvas = GetComponentInParent<Canvas> ();
		if (canvas != null) scale = canvas.scaleFactor;

		MovementRange *= scale;
	}

	private bool AxisExists(string axisName) {
	
		try {
			Input.GetAxis(axisName);
		} catch {
			return false;
		}

		return true;
	}

	private float horizontal;
	private float vertical;

	void Update() {
	
		if (horizontalAxisExists && Input.GetAxis (horizontalAxisName) != 0 && m_UseX) {
			
			if (invertX)
				m_HorizontalVirtualAxis.Set (-sens * Input.GetAxis (horizontalAxisName));
			else
				m_HorizontalVirtualAxis.Set (sens * Input.GetAxis (horizontalAxisName));
			
		} else if (!pointerDown) {
			m_HorizontalVirtualAxis.Set (0);
		}

		if (verticalAxisExists && Input.GetAxis (verticalAxisName) != 0 && m_UseY) {

			if (invertY)
				m_VerticalVirtualAxis.Set (-sens * Input.GetAxis (verticalAxisName));
			else
				m_VerticalVirtualAxis.Set (sens * Input.GetAxis (verticalAxisName));

		} else if (!pointerDown) {
			if (m_UseY) m_VerticalVirtualAxis.Set (0);
		}
	}

	private bool pointerDown;

	public void OnPointerDown(PointerEventData data) {

		startPos = data.position;

		border.gameObject.SetActive (true);
		joystick.gameObject.SetActive (true);

		border.anchoredPosition = data.position / scale;
		joystick.anchoredPosition = data.position / scale;

		joystick.position -= origin;
		border.position -= origin;

		pointerDown = true;
	}

	public void OnDrag(PointerEventData data) {

		Vector2 delta = Vector2.zero;

		if (m_UseX)
		{
			delta.x = data.position.x - startPos.x;
			delta.x = Mathf.Clamp(delta.x, - MovementRange, MovementRange);
		}

		if (m_UseY)
		{
			delta.y = data.position.y - startPos.y;
			delta.y = Mathf.Clamp(delta.y, -MovementRange, MovementRange);
		}

		Vector2 dir = data.position - startPos;
		float dist = dir.magnitude;
		if (dist > MovementRange)
			dir = MovementRange * dir.normalized;
		joystick.anchoredPosition = (startPos + dir) / scale;

		delta.x = -delta.x;
		delta /= MovementRange;

		if (m_UseX)
		{
			if(invertX)
				m_HorizontalVirtualAxis.Set(sens*delta.x);
			else 
				m_HorizontalVirtualAxis.Set(-sens*delta.x);
		}

		if (m_UseY)
		{
			if(invertY) 
				m_VerticalVirtualAxis.Set(-sens*delta.y);
			else
				m_VerticalVirtualAxis.Set(sens*delta.y);
		}

		joystick.position -= origin;
		//border.position -= origin;
	}

	public void OnPointerUp(PointerEventData data) {
	
		if (hide) border.gameObject.SetActive (false);
		if (hide) joystick.gameObject.SetActive (false);

		joystick.anchoredPosition = border.anchoredPosition;

		if (returnPos) border.anchoredPosition = borderPos;
		if (returnPos) joystick.anchoredPosition = joystickPos;

		m_HorizontalVirtualAxis.Set(0);
		if (m_UseY) m_VerticalVirtualAxis.Set(0);

		pointerDown = false;
	}

}
