﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoofRemover : MonoBehaviour
{
    public Collider frontSide;
    public Collider backSide;

    public List<GameObject> toDisable;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter (Collider other)
    {
        var goose = other.GetComponent<GooseController>();
        if (goose == null) return;

        if (Vector3.Dot(goose.transform.position - transform.position, transform.forward) < 0) return;

        for (int i = 0; i < toDisable.Count; i++) toDisable[i].SetActive(false);
    }

    private void OnTriggerExit (Collider other)
    {
        var goose = other.GetComponent<GooseController>();
        if (goose == null) return;

        if (Vector3.Dot(goose.transform.position - transform.position, transform.forward) < 0) return;

        for (int i = 0; i < toDisable.Count; i++) toDisable[i].SetActive(true);
    }
}
