﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;

public class AdsSettings : MonoBehaviour {

    [HideInInspector]
	public List<string> scenesList;

	[HideInInspector]
	public List<bool> showAds;

	[HideInInspector]
	public List<bool> showTopBanner;

	[HideInInspector]
	public bool showAdsOnLoad = true;

    private Dictionary<string, string> settings = new Dictionary<string, string>();

	public bool ready { get; private set; }

    void Awake() {

		ready = true;

		//LoadRemoteSettings ();
	}

	private void StorePrefs() {

		//StoreBool ("Ads.showAdsOnReload", showAdsOnLoad);

		for (int i = 0; i < showAds.Count; i++) {
			StoreBool ("Ads.showAds" + i, showAds [i]);
		}

		for (int i = 0; i < showTopBanner.Count; i++) {
			StoreBool ("Ads.showTopBanner" + i, showTopBanner [i]);
		}
	}

	private void RestorePrefs() {

		if(showAds == null) showAds = new List<bool>();
		if(showTopBanner == null) showTopBanner = new List<bool>();

		//showAdsOnLoad = RestoreBool ("Ads.showAdsOnReload");

		for (int i = 0; i < showAds.Count; i++) {
			showAds [i] = RestoreBool ("Ads.showAds" + i);
		}

		for (int i = 0; i < showTopBanner.Count; i++) {
			showTopBanner [i] = RestoreBool ("Ads.showTopBanner" + i);
		}

	}

	private void StoreBool(string key, bool b) {

		if (b)
			PlayerPrefs.SetInt (key, 1);
		else
			PlayerPrefs.SetInt (key, 0);
	}

	private bool RestoreBool(string key) {

		return PlayerPrefs.GetInt (key, 1) == 1;
	}

	/*private bool quiting = false;
	private bool coroutineRunning = false;
	void OnApplicationQuit() {

		Log ("Try to quit " + quiting);

		if (!quiting) {
			Application.CancelQuit ();
			if(!coroutineRunning) StartCoroutine (DelayedQuit ());
		}
	}

	IEnumerator DelayedQuit() {

		Log ("Try to show last video before quiting");

		coroutineRunning = true;
		TryToShowVideoBanner ();
		yield return new WaitForSeconds(1);
		quiting = true;
	}//*/

	public GMADownloader.ProgressProvider DownloadProgress;

    private void AddSetting(string key, string val) {

        if (!settings.ContainsKey(key)) settings.Add(key, "");

        settings[key] = val;
    }

	public T GetValue<T> (string key, T defaultVal) {

		//Debug.Log ("Get value " + key + " " + typeof(T) + " " + settings.ContainsKey (key));
		if (!settings.ContainsKey (key))
			return defaultVal;

		string val = settings [key];

		//Debug.Log ("Val " + val);

		System.Type type = typeof(T);

		if (type == typeof(bool)) {
			int b = 1;
			if (int.TryParse (val, out b))
				return (T)(object)(b == 1);
			else
				return defaultVal;
		} else if (type == typeof(int)) {
			int v = (int)(object)defaultVal; 
			if (int.TryParse (val, out v))
				return (T)(object)v;
			else
				return defaultVal;
		} else if (type == typeof(float)) {
			float f = (float)(object)defaultVal;
			if (float.TryParse (val, out f))
				return (T)(object)f;
			else
				return defaultVal;
		} else if (type == typeof(string)) {
			return (T)(object)settings [key];
		}

		return defaultVal;
	}

    private void ApplySettings(string settingsStr) {

        this.settings.Clear();

		//Debug.Log ("Parse meta");
		//Debug.Log (settingsStr);

        Dictionary<string, string> settings = ParseMeta(settingsStr);

		if (settings.ContainsKey ("IMAGEVER")) {
			int v = -1;
			int.TryParse(settings["IMAGEVER"], out v);
			if(imageVer < v) AddSetting("UPDATEIMAGE", "1");
			else AddSetting("UPDATEIMAGE", "0");
		}

        if (settings.ContainsKey("SHOWADS"))
        {
            string arrayStr = settings["SHOWADS"];
            arrayStr = arrayStr.Trim();
            arrayStr = arrayStr.Replace(" ", "");
            string[] values = arrayStr.Split(',');
            for (int i = 0; i < showAds.Count && i < values.Length; i++)
            {
                int v = 1;
                int.TryParse(values[i], out v);
                showAds[i] = v == 1;
				AddSetting("SHOWADS" + i, values[i]);
            }
            for (int i = showAds.Count; i < values.Length; i++)
            {
                int v = 1;
                int.TryParse(values[i], out v);
                showAds.Add(v == 1);
				AddSetting("SHOWADS" + i, values[i]);
            }
			settings.Remove ("SHOWADS");
        }

        if (settings.ContainsKey("SHOWTOPBANNER"))
        {
            string arrayStr = settings["SHOWTOPBANNER"];
            arrayStr = arrayStr.Trim();
            arrayStr = arrayStr.Replace(" ", "");
            string[] values = arrayStr.Split(',');
            for (int i = 0; i < showTopBanner.Count && i < values.Length; i++)
            {
                int v = 1;
                int.TryParse(values[i], out v);
                showTopBanner[i] = v == 1;
				AddSetting("SHOWTOPBANNER" + i, values[i]);
            }
            for (int i = showTopBanner.Count; i < values.Length; i++)
            {
                int v = 1;
                int.TryParse(values[i], out v);
                showTopBanner.Add(v == 1);
				AddSetting("SHOWTOPBANNER" + i, values[i]);
            }
			settings.Remove ("SHOWTOPBANNER");
        }

		foreach (var kvp in settings)
			AddSetting (kvp.Key, kvp.Value);
		
        StorePrefs();

        PlayerPrefs.SetInt("AdsControllerFirstLaunch", 0);

		ready = true;

		//Debug.Log ("AdsSettings (" + this.settings.Count + ") :");
		//foreach (var kvp in this.settings) Debug.Log (kvp.Key + " " + kvp.Value);
    }

    private void LoadLocalSettings() {
	
		string path = Application.persistentDataPath + "/Files/AdsSettings.xml";

		//string settingsStr = "";

		if(System.IO.File.Exists(path) == true) {
			//settingsStr = System.IO.File.ReadAllText (path);
		}

		ApplySettings ("");
        //ApplySettings(settingsStr);
    }

	public string adsSettingsURL = "";
	private void LoadRemoteSettings() {
	
		string url = adsSettingsURL + Application.identifier;
		#if UNITY_ANDROID
		url += "&market=android";
		#elif UNITY_IOS
		url += "&market=ios";
		#endif

		//Debug.Log ("Load remote " + url);

		DownloadProgress = GMADownloader.Instance ().Download (url, DownloadFinished, DownloadError);
	}

	private int imageVer = -1;
    private void DownloadFinished(object result) {
		
		DownloadProgress = null;
		
		if(!(result is WWW)) return;
		
		WWW www = (WWW)result;
		
		string write_path = Application.persistentDataPath + "/Files/AdsSettings.xml";
		
		if(System.IO.File.Exists(write_path) == true) {
			string oldMeta = System.IO.File.ReadAllText (write_path);
			Dictionary<string, string> meta = ParseMeta (oldMeta);
			if (meta.ContainsKey ("IMAGEVER")) {
				int v = -1;
				int.TryParse (meta ["IMAGEVER"], out v);
				imageVer = v;
			} 
			//else Debug.LogError ("Not contains");
			System.IO.File.Delete(write_path);
		}
		
		if (System.IO.Directory.Exists (Application.persistentDataPath + "/Files") == false)
			System.IO.Directory.CreateDirectory (Application.persistentDataPath + "/Files");
		
		System.IO.File.WriteAllBytes (write_path, www.bytes);

		ApplySettings (www.text);
	}

    private void DownloadError(object result) {
		
		DownloadProgress = null;
		
		if(!(result is WWW)) return;

		//WWW www = (WWW)result;

		//Log ("[DownloadError] " + www.url + " failed:" + www.error);

		LoadLocalSettings ();
	}

	private Dictionary<string, string> ParseMeta(string data)
	{
		Dictionary<string, string> metaData = new Dictionary<string, string>();

		XmlDocument xmlDocument = new XmlDocument();
		try{
			xmlDocument.LoadXml (data);
		} catch {
			//Debug.Log("[ParseMeta] XML empty");
			return metaData;
		}
		XmlNodeList metaNodes = xmlDocument.GetElementsByTagName("META");
		foreach (XmlNode metaNode in metaNodes) {
			foreach (XmlNode node in metaNode.ChildNodes) {
				if(node.InnerText != "") metaData.Add (node.LocalName, node.InnerText);
			}
		}
		
		//foreach (string key in metaData.Keys) GMALogger.Log (key + " = " + metaData [key]);
		
		//Log ("[ParseMeta] Parsing finished");
		
		return metaData;
	}

	public bool ShouldShowTopBanner (int level) {
	
		if (level < showTopBanner.Count)
			return showTopBanner [level];
		else
			return true;
	}

	public bool ShouldShowInterstitial (int level) {
	
		if (!showAdsOnLoad) return false;

		if (level < showAds.Count) return showAds [level];

		return true;
	}
}
