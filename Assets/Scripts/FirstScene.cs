﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        AdsController.Instance().ShowTopBanner();
        Invoke("Load", 1f);
    }

    private void Load ()
    {
        Application.LoadLevel(1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
