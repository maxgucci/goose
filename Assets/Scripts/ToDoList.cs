﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToDoList : MonoBehaviour
{
    public List<Text> lines;

    public TaskManager manager;

    private bool update;

    public int counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(update) lines[0].transform.parent.gameObject.SetActive(false);
    }

    public void Open ()
    {
        gameObject.SetActive(true);

        ApplyNumLines(manager.tasks.Count);

        for(int i = 0; i < manager.tasks.Count; i++)
        {
            lines[i].text = manager.tasks[i].description;
            if(manager.tasks[i].done)
            {
                lines[i].GetComponentInChildren<Image>(true).gameObject.SetActive(true);
            }
            else
            {
                lines[i].GetComponentInChildren<Image>(true).gameObject.SetActive(false);
            }
        }

        var pad = lines[0].transform.parent.GetComponent<VerticalLayoutGroup>().padding;
        //pad.left = 30;
        lines[0].transform.parent.GetComponent<VerticalLayoutGroup>().padding = pad;
        //update = true;

        counter++;
        if (counter % 3 == 0) AdsController.Instance().TryToShowLargeBanner();
    }


    private void LateUpdate()
    {
        if (update)
        {
            lines[0].transform.parent.gameObject.SetActive(true);
            update = false;
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void ApplyNumLines (int n)
    {
        for(int i = lines.Count; i < n; i++)
        {
            var line = Clone(lines[0].transform);
            var text = line.GetComponent<Text>();

            lines.Add(text);
        }

        for(int i = 0; i < n; i++)
        {
            lines[i].gameObject.SetActive(true);
        }

        for(int i = n; i < lines.Count; i++)
        {
            lines[i].gameObject.SetActive(false);
        }
    }

    private Transform Clone(Transform prefab)
    {
        var clone = Instantiate(prefab);

        clone.SetParent(prefab.parent);
        clone.localScale = prefab.localScale;

        return clone;
    }
}
