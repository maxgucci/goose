%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Head
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Arm_goose
    m_Weight: 1
  - m_Path: Arm_goose/Bone
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2/neck3
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2/neck3/neck4
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2/neck3/neck4/head
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2/neck3/neck4/head/mouth
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine1/meck1/neck2/neck3/neck4/head/mouth/mouth_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.l
    m_Weight: 0
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.l/leg2.l
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.l/leg2.l/leg3.l
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.l/leg2.l/leg3.l/leg3.l_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.r
    m_Weight: 0
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.r/leg2.r
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.r/leg2.r/leg3.r
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/leg1.r/leg2.r/leg3.r/leg3.r_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/spine3
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/spine3/tail
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/spine2/spine3/tail/tail_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.l
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.l/wing1.l
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.l/wing1.l/wing2.l
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.l/wing1.l/wing2.l/wing2.l_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.r
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.r/wing1.r
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.r/wing1.r/wing2.r
    m_Weight: 1
  - m_Path: Arm_goose/Bone/Bone.001/wing.r/wing1.r/wing2.r/wing2.r_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/feet.l
    m_Weight: 0
  - m_Path: Arm_goose/Bone/feet.l/feet.l_end
    m_Weight: 1
  - m_Path: Arm_goose/Bone/feet.r
    m_Weight: 0
  - m_Path: Arm_goose/Bone/feet.r/feet.r_end
    m_Weight: 1
  - m_Path: goose_midle
    m_Weight: 0
