﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Person : MonoBehaviour
{
    public Animator anim;

    public NavMeshAgent agent;

    public List<Transform> waypoints;

    public float walkSpeed = 1.5f;
    public float runSpeed = 3f;

    private bool waiting;

    public List<Item> items;

    private Item targetItem;
    private List<Item> targetItems = new List<Item>();
    private List<Item> takedItems = new List<Item>();

    // Start is called before the first frame update
    void Start()
    {
        agent.speed = walkSpeed;
        agent.SetDestination(transform.position);

        for (int i = 0; i < items.Count; i++) items[i].onTake += OnItemTaked;
    }

    // Update is called once per frame
    void Update()
    {
        if(targetItems.Count > 0 && targetItem == null)
        {
            targetItem = targetItems[0];
            targetItems.RemoveAt(0);
            agent.speed = runSpeed;
            CancelInvoke("SetDestination");
            waiting = false;
        }

        if (targetItem != null)
        {
            if (Mathf.Approximately(targetItem.DistanceFromPlace(), 0))
            {
                targetItem = null;
                agent.speed = walkSpeed;
                agent.SetDestination(transform.position);
            }
            else
            {
                agent.SetDestination(targetItem.transform.position);
                Vector3 dir = targetItem.transform.position - transform.position;
                dir.y = 0;
                if (dir.magnitude < 0.9f)
                {
                    targetItem.Return();
                    takedItems.Add(targetItem);
                    targetItem.gameObject.SetActive(false);
                    if (GameController.instance.goose.onItemDroped != null) GameController.instance.goose.onItemDroped(targetItem);
                    targetItem = null;
                    agent.speed = walkSpeed;
                    AdsController.Instance().TryToShowLargeBanner();
                }
            }
        }
        else if(takedItems.Count > 0)
        {
            var targetItem = takedItems[0];
            agent.SetDestination(targetItem.transform.position);
            Vector3 dir = targetItem.transform.position - transform.position;
            dir.y = 0;
            if (dir.magnitude < 0.5f)
            {
                targetItem.gameObject.SetActive(true);
                targetItem.Return();
                takedItems.Remove(targetItem);
            }
        }
        else if (!waiting && Vector3.Distance(transform.position, agent.destination) < 0.1f) OnDestinationReached();

        float v = agent.velocity.magnitude / walkSpeed;

        anim.SetFloat("Speed", v);
    }

    private void OnDestinationReached()
    {
        waiting = true;

        Invoke("SetDestination", 3f);
    }

    private void SetDestination ()
    {
        waiting = false;

        if (waypoints.Count == 0) return;

        agent.SetDestination(waypoints[Random.Range(0, waypoints.Count)].position);
    }

    private void OnItemTaked(Item item)
    {
        targetItems.Add(item);
    }
}
