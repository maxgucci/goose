﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    private void Awake()
    {
        instance = this;
    }

    public GooseController goose;

    public TaskManager tasker;

    public ToDoList list;

    // Start is called before the first frame update
    void Start()
    {
        AdsController.Instance().ShowTopBanner();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTaskCompleted ()
    {
        int n = 0;
        for(int i = 0; i < tasker.tasks.Count; i++)
        {
            if (!tasker.tasks[i].done) n++;
        }

        if(n == 0)
        {
            tasker.Prepare();
            tasker.Generate();
            list.counter = 0;
            list.Open();
        }

        AdsController.Instance().TryToShowVideoBanner();
    }
}
