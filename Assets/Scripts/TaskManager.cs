﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskManager : MonoBehaviour
{
    [System.Serializable]
    public class Task
    {
        public TaskType type;

        public string description;

        public Item item;
        public Interface interf;

        public Transform point;

        public float time;

        public bool done;
    }

    public List<Task> tasks;

    public List<Item> insideItems;
    public List<Item> outsideItems;
    private List<Item> allItems = new List<Item>();

    public List<Interface> interfaces;

    public List<Transform> outsidePoints;
    public List<Transform> insidePoints;
    private List<Transform> allPoints = new List<Transform>();

    public BoxCollider houseCollider;

    private Task currentTask;
    public GameObject targetPoint;

    public string info;

    private float timer;

    public Text lblTimer;

    public int maxTasks = 9;

    private List<Outline> highlightedObjects = new List<Outline>();

    public RectTransform sign;

    public RectTransform canvas;

    public enum TaskType
    {
        TakeOut,
        TakeIn,
        Courier,
        Timer,
        SwitchOnOff,
    }

    // Start is called before the first frame update
    void Start()
    {
        GameController.instance.goose.onItemTaked += OnItemTaked;
        GameController.instance.goose.onItemDroped += OnItemDroped;
        GameController.instance.goose.onInterracted += OnInterracted;

        allPoints.AddRange(outsidePoints);
        allPoints.AddRange(insidePoints);

        allItems.AddRange(insideItems);
        allItems.AddRange(outsideItems);

        Prepare();

        Generate();
    }

    public void Prepare ()
    {
        insideItems.Clear();
        outsideItems.Clear();

        for(int i = 0; i < allItems.Count; i++)
        {
            if(houseCollider.bounds.Contains(allItems[i].transform.position))
            {
                insideItems.Add(allItems[i]);
            }
            else
            {
                outsideItems.Add(allItems[i]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        info = "No task";
        if (currentTask != null) info = currentTask.description + " " + currentTask.done;

        if (currentTask != null && !currentTask.done)
        {
            if (currentTask.type == TaskType.Courier || currentTask.type == TaskType.TakeIn || currentTask.type == TaskType.TakeOut)
            {
                if (!targetPoint.activeSelf) targetPoint.SetActive(true);

                targetPoint.transform.position = currentTask.point.position + 0.1f * Vector3.up;
            }

            if (currentTask.type == TaskType.SwitchOnOff)
            {
                if (targetPoint.activeSelf) targetPoint.SetActive(false);
            }

            if (currentTask.type == TaskType.Timer)
            {
                if (targetPoint.activeSelf) targetPoint.SetActive(false);
                if (!lblTimer.gameObject.activeSelf) lblTimer.gameObject.SetActive(true);

                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    currentTask.done = true;
                    GameController.instance.OnTaskCompleted();
                }
                lblTimer.text = "" + (int)timer;
            }

            if (currentTask.point != null)
            {
                if (!sign.gameObject.activeSelf) sign.gameObject.SetActive(true);

                var pos = Camera.main.WorldToScreenPoint(currentTask.point.position);

                pos.x /= canvas.localScale.x;
                pos.y /= canvas.localScale.y;

                float w = -30 + Screen.width / canvas.localScale.x;
                float h = -30 + Screen.height / canvas.localScale.y;

                info += " | " + pos;

                if (pos.x > w) pos.x = w;
                if (pos.x < 30) pos.x = 30;
                if (pos.y > h) pos.y = h;
                if (pos.y < 30) pos.y = 30;

                info += " | " + pos;

                sign.anchoredPosition = pos;

                Vector3 dir = sign.localPosition;
                sign.localRotation = Quaternion.LookRotation(Vector3.forward, dir);
            }
            else if (sign.gameObject.activeSelf) sign.gameObject.SetActive(false);

        }
        else
        {
            if (targetPoint.activeSelf) targetPoint.SetActive(false);
            if (lblTimer.gameObject.activeSelf) lblTimer.gameObject.SetActive(false);
            if (sign.gameObject.activeSelf) sign.gameObject.SetActive(false);
        }
    }

    public void OnItemTaked (Item item)
    {
        Debug.Log("Item taked " + item, item);

        for(int i = 0; i < tasks.Count; i++)
        {
            if (tasks[i].done) continue;

            if (tasks[i].item == item)
            {
                currentTask = tasks[i];
                Debug.Log("Found task " + currentTask.description);
            }
        }

        if (currentTask != null && currentTask.type == TaskType.Timer) timer = currentTask.time;
    }

    public void OnItemDroped (Item item)
    {
        Debug.Log("Drop " + item, item);

        if (currentTask != null && currentTask.item == item)
        {
            if(currentTask.type == TaskType.Courier || currentTask.type == TaskType.TakeIn || currentTask.type == TaskType.TakeOut)
            {
                Vector3 dir = currentTask.point.position - item.transform.position;
                dir.y = 0;
                if (dir.magnitude < 1f)
                {
                    Destroy(currentTask.item.GetComponent<Outline>());
                    currentTask.done = true;
                    GameController.instance.OnTaskCompleted();
                }
            }

            currentTask = null;
        }
    }

    private void OnInterracted (Interface interf)
    {
        for(int i = 0; i < tasks.Count; i++)
        {
            if (tasks[i].done) continue;
            if (tasks[i].interf == interf)
            {
                Destroy(tasks[i].interf.GetComponent<Outline>());
                tasks[i].done = true;
                GameController.instance.OnTaskCompleted();
            }
        }
    }

    public void Generate ()
    {
        tasks.Clear();

        for(int i = 0; i < highlightedObjects.Count; i++)
        {
            Destroy(highlightedObjects[i]);
        }

        highlightedObjects.Clear();

        int N = System.Enum.GetNames(typeof(TaskType)).Length;

        List<Item> insideItems = new List<Item>(this.insideItems);
        List<Item> outsideItems = new List<Item>(this.outsideItems);
        List<Item> allItems = new List<Item>(this.allItems);
        List<Interface> interfaces = new List<Interface>(this.interfaces);

        for(int i = 0; i < 50; i++)
        {
            Task t = null;// new Task();

            TaskType type = (TaskType)Random.Range(0, N);

            if (type == TaskType.Courier && allItems.Count > 0)
            {
                t = new Task();
                t.item = allItems[Random.Range(0, allItems.Count)];
                t.point = allPoints[Random.Range(0, allPoints.Count)];

                t.description = "Взять " + t.item.RussianName + " и отнести в указанную точку";
                if (Application.systemLanguage != SystemLanguage.Russian) t.description = "Find and move item " + t.item.Name + " to the specified point";
                allItems.Remove(t.item);
                if (insideItems.Contains(t.item)) insideItems.Remove(t.item);
                if (outsideItems.Contains(t.item)) outsideItems.Remove(t.item);
            }

            if (type == TaskType.SwitchOnOff && interfaces.Count > 0)
            {
                t = new Task();
                t.interf = interfaces[Random.Range(0, interfaces.Count)];
                t.description = "Включить " + t.interf.RussianName;
                if (Application.systemLanguage != SystemLanguage.Russian) t.description = "Switch on " + t.interf.Name;
                interfaces.Remove(t.interf);
            }

            if (type == TaskType.TakeIn && outsideItems.Count > 0)
            {
                t = new Task();
                t.item = outsideItems[Random.Range(0, outsideItems.Count)];
                t.point = insidePoints[Random.Range(0, insidePoints.Count)];

                t.description = "Занести " + t.item.RussianName + " в дом";
                if (Application.systemLanguage != SystemLanguage.Russian) t.description = "Move item " + t.item.Name + " from yard to the house";
                outsideItems.Remove(t.item);
                if (insideItems.Contains(t.item)) insideItems.Remove(t.item);
                if (allItems.Contains(t.item)) allItems.Remove(t.item);
            }

            if (type == TaskType.TakeOut && insideItems.Count > 0)
            {
                t = new Task();
                t.item = insideItems[Random.Range(0, insideItems.Count)];
                t.point = outsidePoints[Random.Range(0, outsidePoints.Count)];

                t.description = "Вынести " + t.item.RussianName + " из дома";
                if (Application.systemLanguage != SystemLanguage.Russian) t.description = "Move item " + t.item.Name + " from house to the yard";
                insideItems.Remove(t.item);
                if (outsideItems.Contains(t.item)) outsideItems.Remove(t.item);
                if (allItems.Contains(t.item)) allItems.Remove(t.item);
            }

            if (type == TaskType.Timer && allItems.Count > 0)
            {
                t = new Task();
                t.item = allItems[Random.Range(0, allItems.Count)];
                t.time = Random.Range(15, 45);
                t.description = "Взять " + t.item.RussianName + " и продержаться " + (int)t.time + " секунд";
                if (Application.systemLanguage != SystemLanguage.Russian) t.description = "Hold item " + t.item.Name + " in the beak for " + (int)t.time + " secs without losing it";
                allItems.Remove(t.item);
                if (insideItems.Contains(t.item)) insideItems.Remove(t.item);
                if (outsideItems.Contains(t.item)) outsideItems.Remove(t.item);
            }

            if(t != null)
            {
                t.type = type;
                tasks.Add(t);
            }
        }

        //Debug.Log(tasks.Count + " " + maxTasks + " | " + (maxTasks + 1) + " " + (tasks.Count - maxTasks));
        if (tasks.Count > maxTasks) tasks.RemoveRange(maxTasks, tasks.Count - maxTasks);

        for(int i = 0; i < tasks.Count; i++)
        {
            if(tasks[i].item != null)
            {
                var o = tasks[i].item.gameObject.AddComponent<Outline>();
                o.OutlineMode = Outline.Mode.OutlineHidden;
                o.OutlineWidth = 3;
                o.OutlineColor = Color.green;
                highlightedObjects.Add(o);
            }
            if(tasks[i].interf != null)
            {
                var o = tasks[i].interf.gameObject.AddComponent<Outline>();
                o.OutlineMode = Outline.Mode.OutlineHidden;
                o.OutlineWidth = 3;
                o.OutlineColor = Color.green;
                highlightedObjects.Add(o);
            }
        }
    }
}
